const Zetabase = require('../index');
/*
One of the data in the database.

"2635029433e316945478598b7f9e4a3b": {
    "kind": "youtube#searchResult",
    "etag": "\"Bdx4f4ps3xCOOo1WZ91nTLkRZ_c/NqS_mkVU-D-Bzax1ogrhc95RN94\"",
    "id": {
        "kind": "youtube#video",
        "videoId": "HR97Uq2Ejks"
    },
    "snippet": {
        "publishedAt": "2019-02-15T00:06:27.000Z",
            "channelId": "UCBBGM84ZOs7z5jpTQAaZ_Hg",
            "title": "How to create a CAMERA APP in VueJS - Day 15 - #100DaysOfCode",
            "description": "In this tutorial, We make a CAMERA APP in VueJS. We use different components and features of Vue to create a cool camera web application. The main ...",
            "thumbnails": {
            "default": {
                "url": "https://i.ytimg.com/vi/HR97Uq2Ejks/default.jpg",
                "width": 120,
                "height": 90
            },
            "medium": {
                "url": "https://i.ytimg.com/vi/HR97Uq2Ejks/mqdefault.jpg",
                "width": 320,
                "height": 180
            },
            "high": {
                "url": "https://i.ytimg.com/vi/HR97Uq2Ejks/hqdefault.jpg",
                "width": 480,
                "height": 360
            }
        },
        "channelTitle": "Tyler Potts",
        "liveBroadcastContent": "none"
    }
}
*/

let db = new Zetabase("./database.json");
db.wipe("/")
for (let i in Data)
    db.append("/Youtube", Data[i])

//Query the kind of the result
let results = db.query("/Youtube/:keys/@kind");
Object.keys(results.Youtube.keys).map(key => {
    console.log(
        "Key:" + key,
        "\t",
        "Kind:" + results.Youtube.keys[key].kind
    )
})

console.log()

//Query all the high iamge urls.
results = db.query("/Youtube/:keys/@snippet.thumbnails.high.url")
Object.keys(results.Youtube.keys).map(key => {
    console.log(
        "Key:" + key,
        "\t",
        "url:" + results.Youtube.keys[key].url
    )
})

console.log()

//query(path, true) will trim all the unnecessary keys.
results = db.query("/Youtube/:keys/@snippet.thumbnails.high.url", true)
Object.keys(results).map(key => {
    console.log(
        "Key:" + key,
        "\t",
        "url:" + results[key].url
    )
})