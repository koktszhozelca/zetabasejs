const Zetabase = require('../index')
const { Model, Indexing, TaskIndexing, Audit, Key } = require("../orm")
const crypto = require("crypto")

// Init Zetabase
const db = new Zetabase("./database.json", { orm: true })

// Prepare models
class User extends Model {
    constructor(name, wallet) {
        super({
            // Specify the index fields.
            // Entity won't index by default.
            index: ["name", "wallet", "__ctime__"]
        })
        this.name = name
        this.wallet = wallet
    }

    static create(name, wallet) {
        // Entity will be indexed automatically.
        return new User(name, wallet).put()
    }
}

class Transaction extends Model {
    constructor(payer, payee, amount) {
        super({
            index: ["payer", "payee", "amount", "__mtime__", "__ctime__"]
        })
        this.payer = payer
        this.payee = payee
        this.amount = amount
    }

    process() {
        let payer = User.get(this.payer)
        let payee = User.get(this.payee)
        payer.wallet -= this.amount
        payee.wallet += this.amount
        Model.puts(payer, payee)
        this.put()
        console.log(
            `
${new Date(this.__mtime__ / 1000).toLocaleTimeString()}: ${payer.name} pays $${this.amount} to ${payee.name}.
            ${payer.name} remains $${payer.wallet}
            ${payee.name} remains $${payee.wallet}
`
        )
    }

    static create(payer, payee, amount) {
        const tx = new Transaction(payer, payee, amount).put()
        tx.process()
        return tx
    }
}

class Report extends Model {
    constructor(user, from, to) {
        super()
        this.user = user
        this.from = from
        this.to = to
    }

    process() {
        const getPrice = (tx) => {
            if (tx.payer === this.user && tx.payee === this.user) return 0
            if (tx.payer === this.user) return -tx.amount
            return tx.amount
        }
        let payTxs = Transaction.query({ filters: { payer: v => v === this.user } })
        let receiveTxs = Transaction.query({ filters: { payee: v => v === this.user } })
        let txs = [...payTxs, ...receiveTxs].sort((a, b) => a.__ctime__ - b.__ctime__)
        return txs.map(tx => { return { 'timestamp': tx.__mtime__, 'price': getPrice(tx) } })
    }

    static create(user, from, to) {
        return new Report(user, from, to).put()
    }
}

// Libs
const ranString = (len = 5, encoding = 'hex') => crypto.randomBytes(len).toString(encoding)
const ranNum = (min = 0, max = 100) => Math.floor(Math.random() * max) + min

// Generate data
const genUsers = (size = 10) => {
    let users = []
    for (let i = 0; i < size; i++)
        users.push(User.create(ranString(), ranNum(100, 1000)))
    return users
}

const pickUsers = (size = 2) => {
    const lastId = auditor.getLastId('User')
    if (!lastId || lastId < 2) return []
    let payerId = ranNum(1, lastId)
    let payeeId = ranNum(1, lastId)
    while (payeeId == payerId || !payeeId) {
        payeeId = (payeeId + 1) % lastId
        payeeId = payeeId ? payeeId : payeeId + 1
    }
    let payer = new Key("User", payerId).b64()
    let payee = new Key("User", payeeId).b64()
    return { payer, payee }
}

const genTransaction = (payerKey, payeeKey) => {
    let payer = User.get(payerKey)
    let amount = ranNum(1, payer.wallet * 0.8)
    return Transaction.create(payerKey, payeeKey, amount)
}

// db.wipe('/')
// const auditor = Audit.getInstance()

// let users = genUsers(5)
// console.table(users.map(u => { return { "Name": u.name, "Wallet ($HKD)": u.wallet, "Key": Key.parse(u.__key__) } }))

// for (let i = 0; i < 10; i++) {
//     let { payer, payee } = pickUsers()
//     genTransaction(payer, payee)
// }

// users = User.query()
// console.table(users.map(u => { return { "Name": u.name, "Wallet ($HKD)": u.wallet, "Key": Key.parse(u.__key__) } }))

// let report = new Report(new Key("User", 1).b64(), 1, 1).process()
// console.log(report)

// const task = new TaskIndexing("./SortIndex.json")
// task.run()

// let txs = Transaction.query({ sortIndex: "amountDesc", reverse: true })
// console.table(txs.map(tx => { return { mtime: tx.__mtime__, ctime: tx.__ctime__, amount: tx.amount } }))

// let users = User.query({ sortIndex: "walletDesc" })
// console.table(users.map(u => { return { Name: u.name, Wallet: u.wallet } }))