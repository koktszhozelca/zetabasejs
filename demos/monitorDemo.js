const Zetabase = require('../index');

let db = new Zetabase("./database.json");
db.wipe("/")
let aliceKey = null
// Set monitor 
db.monitor('/School/RoomA/Students', (std, actCode) => {
    console.log("ACT", actCode)
    if(actCode === "APPEND") {
        console.log("A student is added to RoomA: \n", JSON.stringify(std, null, 2), actCode)
        aliceKey = std.key
    } else console.log("Removed a student from RoomA: \n", JSON.stringify(std, null, 2), actCode)
})
db.monitor('/School/RoomA/Name', (name, actCode) => console.log("Room name is changed to ", name, actCode))
db.monitor('/School/RoomA/Description', (desc, actCode) => console.log("Room description is changed to ", desc, actCode))

db.append('/School/RoomA/Students', { name: 'Alice', age: 10 })
// A student is added to RoomA: 
//  {
//   "name": "Alice",
//   "age": 10,
//   "key": "848e410b347b81645be45a9418095d76"
// } APPEND
db.write('/School/RoomA/Name', 'R001')
// Room name is changed to  R001 WRITE
db.write('/School/RoomA/Description', 'This room is for kids');
// Room description is changed to  This room is for kids WRITE
db.wipe('/School/RoomA/Students/'+aliceKey)
// Removed a student from RoomA: 
//  {
//   "name": "Alice",
//   "age": 10,
//   "key": "7c6129dd722d0eb1f3f3ce91f18cb39f"
// } WIPE