const Model = require("./Model")

class __Indexing__ extends Model {
    constructor() {
        super({ audit: false })
        this.data = {}
    }

    set(cls, index, processed) {
        if (!this.data[cls]) this.data[cls] = {}
        this.data[cls][index] = processed
        this.put()
    }

    unset(cls) {
        delete this.data[cls]
        this.put()
    }

    static sort(cls, index, data, options = {}) {
        try {
            const indices = this.getInstance().data[cls][index]
            let res = data.sort((a, b) => options.keys_only ? this._sortKeys(indices, a, b) : this._sortEntities(indices, a, b))
            return options.reverse ? res.reverse() : res
        } catch (err) {
            console.error(`Error: Index not found for class ${cls}, index: ${index}`)
            return []
        }
    }

    static _sortKeys(indices, a, b) {
        return indices.indexOf(a) - indices.indexOf(b)
    }

    static _sortEntities(indices, a, b) {
        return indices.indexOf(a.__key__) - indices.indexOf(b.__key__)
    }

    static getInstance() {
        return __Indexing__.exist() ? __Indexing__.query()[0] : new __Indexing__().put()
    }
}

module.exports = __Indexing__