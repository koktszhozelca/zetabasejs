/*
    Indexing file format
    {
        "User": [
            {
                "sortByNameAsc": [
                    { "name.en": "asc" },
                    { "__ctime__": "desc" }
                ]
            },
            {
                "sortByAgeDesc": [
                    { "age": "desc" },
                    { "__ctime__": "desc" }
                ]
            }
        ]
    }
*/
const fs = require("fs")
const Indexing = require("../Indexing")
const Audit = require("../Audit")

class TaskIndexing {
    constructor(specPath) {
        this.spec = JSON.parse(fs.readFileSync(specPath))
        this.auditor = Audit.getInstance()
        this.indexer = Indexing.getInstance()
    }

    run() {
        let statistics = []
        Object.keys(this.spec).map(cls => {
            if (!this.auditor.indices[cls]) return
            this.spec[cls].map(index => {
                let name = Object.keys(index)[0]
                let fields = Object.values(index[name])
                let docs = Object.values(this.auditor.indices[cls])
                let processed = docs.sort((a, b) => this._parseField(fields, a, b)).map(doc => doc.docKey)
                statistics.push({ cls, name, size: processed.length })
                this.indexer.set(cls, name, processed)
            })
        })
        statistics.map(({ cls, name, size }) => console.log(`${cls}, Index: ${name} --> ${size} docs.`))
    }

    _parseField(fields, a, b, ptr = 0) {
        let field = fields[ptr]
        let key = Object.keys(field)[0]
        let order = field[key] === "asc" ? -1 : 1
        if (a.values[key] < b.values[key])
            return order
        else if (a.values[key] === b.values[key] && fields[ptr + 1])
            return this._parseField(fields, a, b, ptr + 1)
        return order === -1 ? 1 : -1
    }
}

module.exports = TaskIndexing