const Model = require("./Model")
const Key = require("./Key")

var __instance__ = null
class __Audit__ extends Model {
    constructor() {
        super({ audit: false, unique: true })
        this.logs = {}
        this.last = {}
        this.indices = {}
    }

    log(cls) {
        if (!this.last[cls]) this.last[cls] = 0
        if (!this.logs[cls]) this.logs[cls] = []
        let id = ++this.last[cls]
        this.logs[cls].push({ id, ctime: new Date().getTime() * 1000 })
        this.put()
        return id
    }

    index(model) {
        let cls = model.constructor.name
        if (!this.indices[cls]) this.indices[cls] = {}
        const doc = { docKey: model.__key__, values: model.getIndexValues() }
        this.indices[cls][doc.docKey] = doc
        this.put()
    }

    unindex(model) {
        let cls = model.constructor.name
        if (!this.indices[cls]) return
        delete this.indices[cls][model.__key__]
        this.put()
    }

    getLastId(cls) {
        return this.last[cls] ? this.last[cls] : 0
    }

    static getInstance() {
        return __Audit__.exist() ? __Audit__.get(new Key("__Audit__", 1).b64()) : new __Audit__().put()
    }
}

module.exports = __Audit__;