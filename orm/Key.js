const crypto = require('crypto')

class Key {
    constructor(cls, id) {
        this.cls = cls;
        this.id = parseInt(id);
        // this.version = parseInt(version || 0); // Versioning requires ancestor
        // this.subId = subId || crypto.randomBytes(10).toString("hex")
    }

    b64() {
        return Buffer.from(`${this.cls}-${this.id}`).toString("base64") + Key.keyPostfix()
    }

    static parse(b64Str) {
        let [_, cls, id] = Buffer.from(b64Str, 'base64').toString().match(/(.+)-(.+)/)
        return new Key(cls, id)
    }

    static keyPostfix(prefix = "ZETABASE_KEY") {
        return crypto.createHash("md5").update(prefix).digest('base64').substr(0, 5).toString('hex')
    }
}

module.exports = Key