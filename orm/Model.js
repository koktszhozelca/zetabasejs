const { Key } = require(".")

var DBCONN = null
var AUDIT = null
var INDEXING = null

class Model {
    static setDBCONN(db) {
        DBCONN = db
    }

    static setAudit(audit) {
        AUDIT = audit
    }

    static setIndexing(indexing) {
        INDEXING = indexing
    }

    constructor(options = { audit: true, index: [], compute: [] }) {
        if (!DBCONN) throw "Error: ORM module is not enabled on ZetabaseJS"
        options = Model._defaultConstructorOptions(options)
        this.__ctime__ = null
        this.__mtime__ = null
        this.__key__ = null
        this.__audit__ = options.audit
        this.__index__ = options.index
        this.__unique__ = options.unique
        this.__compute__ = [...options.compute, '__compute__', '__index__']
    }

    put() {
        if (!this.__key__) {
            if (this.__unique__) this.__key__ = DBCONN._key({ cls: this.constructor.name, id: 1 })
            else if (this.__audit__) this.__key__ = DBCONN._key({ cls: this.constructor.name, id: AUDIT.log(this.constructor.name) })
            else this.__key__ = DBCONN._key()
        }
        if (!this.__ctime__) this.__ctime__ = this.__mtime__ = new Date().getTime() * 1000
        else this.__mtime__ = new Date().getTime() * 1000
        const data = Object.keys(this)
            .filter(key => !this.__compute__.includes(key))
            .reduce((obj, key) => { obj[key] = this[key]; return obj }, {})
        DBCONN.write(`/${this.constructor.name}/${this.__key__}`, data)
        if (this.__index__.length) this.index()
        return this
    }

    getIndexValues(ptr = this, fields = this.__index__) {
        let values = {}
        fields = Array.isArray(fields) ? fields : [fields]
        fields.map(field => {
            if (field instanceof Object) values[field.label] = field.compute(this)
            else if (field.indexOf('.') < 0) values[field] = ptr[field]
            else {
                const cur = field.match(/(.+?)\./)[1]
                const nxt = field.match(`${cur}\.(.+)`)[1]
                values[field] = this.getIndexValues(ptr[cur], nxt)[nxt]
            }
        })
        return values
    }

    index() {
        AUDIT.index(this)
    }

    unindex() {
        AUDIT.unindex(this)
    }

    static get(key) {
        if (!DBCONN) throw "Error: Please initialize an ZetabaseJS instance with option { orm: true }."
        return Object.assign(new this(), DBCONN.read(`/${this.name}/${key}`))
    }

    static query(options) {
        if (!DBCONN) throw "Error: Please initialize an ZetabaseJS instance with option { orm: true }."
        if (!DBCONN.containsKey(this.name)) return null;
        options = this._defaultQueryOptions(options || {})
        let results = DBCONN.query(`/${this.name}/:key/@${options.fields}`, options.filters)
        if (options.sortIndex) results = INDEXING.sort(this.name, options.sortIndex, results, options)
        return results && options.get ? results.map(res => this.get(res.__key__)) : results
    }

    static _defaultQueryOptions(options) {
        if (!options.fields) options.fields = "{}"
        if (isNaN(options.get)) options.get = true
        if (options.get && options.fields.indexOf('__key__') < 0) options.fields = options.fields.replace('}', ', __key__ }')
        if (!options.filters) options.filters = val => true
        return options
    }

    static _defaultConstructorOptions(options) {
        if (options.audit === undefined) options.audit = true
        if (!options.index) options.index = []
        if (!options.compute) options.compute = []
        if (!options.unique) options.unique = null
        return options
    }

    static exist(b64Str = null) {
        return b64Str ? DBCONN.containsKey(`/${Key.parse(b64Str).cls}/${b64Str}`) : DBCONN.containsKey(`/${this.name}`)
    }

    static puts(...models) {
        return models.map(model => model.put())
    }
}

module.exports = Model;