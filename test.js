const Zetabase = require(".");

//Init the database with a filename.
let db = new Zetabase("./database.json");

//Set the write buffer time, default 10 (10 ms)
//Prevent write too frequent
db.setWriteBufferTime(100 * 1)

//Delete data
db.wipe("/");

//Write data to path directly
db.write("/RoomA/Info", { maxStudentCount: 10, description: "Regular classroom" });
console.log();

let roomInfo = db.read("/RoomA/Info");
console.log("Room info: ", roomInfo);
console.log();

//Check if key exist
let isExist = db.containsKey("/RoomA");
console.log("Contains /RoomA", isExist);
console.log();

isExist = db.containsKey("/NotExist");
console.log("Contains /NotExist", isExist);
console.log();

//Append data to the path, a key will be generated accordingly.
let key = db.append("/RoomA/Student", { name: "Tom", id: 15024012, gender: "M", active: false });
console.log("Insert Tom with key: ", key);

db.append("/RoomA/Student", { name: "Peter", id: 15024013, gender: "M", active: false });
db.append("/RoomA/Student", { name: "Alice", id: 15024014, gender: "F", active: false });
db.append("/RoomA/Student", { name: "Ada", id: 15024015, gender: "F", active: false });

//Read iteratively or Update iteratively
console.log();
console.log("Read or update iteratively")
console.log();
db.iterate("/RoomA/Student", (key, student) => {
    console.log("Key: ", key)
    console.log("Student: ", student);
    console.log();
    student.active = true;
})
console.log("The value is updated");
db.iterate("/RoomA/Student", (key, student) => {
    console.log("Key: ", key)
    console.log("Student: ", student);
    console.log();
})

console.log();

db.iterate("/RoomA/Student", (studentID, student, iterator) => {
    db.write("/RoomA/Student/" + studentID + "/Exams/Math", {
        score: Math.floor(Math.random() * 100)
    })
    db.write("/RoomA/Student/" + studentID + "/Exams/History", {
        score: Math.floor(Math.random() * 100)
    })
})

db.iterate("/RoomA/Student", (key, student, exams) => {
    exams.iterate((exam, data, scores) => {
        scores.iterate((subject, result) => {
            console.log(
                `Student name: ${student.name}`,
                `Subject ${subject}`,
                `Score ${result.score}`
            )
        })
        console.log()
    })
})

//The empty data node will be removed automatically
db.write("/A/B/C/D/E/F/G/H", "Message");
console.log("In memory: ", JSON.stringify(db.memory, null, 2));

console.log();

db.write("/A/B/C/D/E", "New message");
console.log("In memory: ", JSON.stringify(db.memory, null, 2));

db.wipe("/A/B/C/D/E/F/G/H");
console.log();
console.log("In memory: ", JSON.stringify(db.memory, null, 2));

//Save immediately, call this function too frequent (<=10ms) may fail.
db.invalidate(true);

let aliceKey = null
// Set monitor 
db.monitor('/School/RoomA/Students', (std, actCode) => {
    console.log("ACT", actCode)
    if(actCode === "APPEND") {
        console.log("A student is added to RoomA: \n", JSON.stringify(std, null, 2), actCode)
        aliceKey = std.key
    } else console.log("Removed a student from RoomA: \n", JSON.stringify(std, null, 2), actCode)
})
db.monitor('/School/RoomA/Name', (name, actCode) => console.log("Room name is changed to ", name, actCode))
db.monitor('/School/RoomA/Description', (desc, actCode) => console.log("Room description is changed to ", desc, actCode))

db.append('/School/RoomA/Students', { name: 'Alice', age: 10 })
// A student is added to RoomA: 
//  {
//   "name": "Alice",
//   "age": 10,
//   "key": "848e410b347b81645be45a9418095d76"
// } APPEND
db.write('/School/RoomA/Name', 'R001')
// Room name is changed to  R001 WRITE
db.write('/School/RoomA/Description', 'This room is for kids');
// Room description is changed to  This room is for kids WRITE
db.wipe('/School/RoomA/Students/'+aliceKey)
// Removed a student from RoomA: 
//  {
//   "name": "Alice",
//   "age": 10,
//   "key": "7c6129dd722d0eb1f3f3ce91f18cb39f"
// } WIPE