const EventEmitter = require('events');
const crypto = require('crypto')

class Observer extends EventEmitter {
    observe(path, callback) {
        this.on(this._hashKey(path), callback);
    }

    check(path, data, actCode, removeLast=false) {
        if(removeLast) path = this._removeLast(path)
        this.emit(this._hashKey(path), data, actCode)
    }    

    _hashKey(str) {
        return this._hash(this._trim(this._split(str)).join('/'))
    }

    _hash(...str) {
        return crypto.createHash('md5').update(str.join()).digest('hex');
    }

    _split(path, delim = '/') {
        if (path.indexOf('/') < 0) return [path];
        return this._trim(path.split('/'));
    }

    _trim(tokens) {
        return tokens.filter(t => t.length)
    }

    _removeLast(str, delim='/') {
        let tokens = str.split(delim);
        return tokens.slice(0, tokens.length-1).join(delim);
    }
}
module.exports = Observer;