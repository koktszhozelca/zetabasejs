class Deprecator {
    static deprecated(oldFuncName, newFuncName) {
        console.error(`[Deprecated] ${oldFuncName} is deprecated, please use ${newFuncName} instead.`);
    }
}
module.exports = Deprecator;