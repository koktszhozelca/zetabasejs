class NodePtr {
    constructor(cur, next) {
        this.cur = cur;
        this.next = next;
    }

    update(nodePtr) {
        this.cur = nodePtr.cur;
        this.next = nodePtr.next;
    }
}
module.exports = NodePtr;