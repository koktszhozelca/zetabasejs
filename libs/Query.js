const DataNode = require("../libs/dataStructures/DataNode");
const KEY_POSTFIX = DataNode.keyPostfix()

class Query {
    static reduce(obj) {
        let keys = Object.keys(obj)
        if (!keys.length) return null
        if (keys.length === 1 && keys[0].substring(keys[0].length - KEY_POSTFIX.length, keys[0].length) !== KEY_POSTFIX) return Query.reduce(obj[keys[0]])
        return keys.filter(key => key.substring(key.length - KEY_POSTFIX.length, key.length) === KEY_POSTFIX).map(key => obj[key])
    }
}
module.exports = Query;